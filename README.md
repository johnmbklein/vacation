# Vacation Destination Picker

#### Choses destinations based on stated user preferences, 2016

#### By John Klein

## Description

This website takes user input on activity and destination preferences and generates suggestions bases on that input.

## Known Bugs

Styling needs improvement.

## Support and contact details

Email me with questions or comments

## Technologies Used

This site was built using Bootstrap, jQuery, and Javascript

### License

Licensed under the GPL. See license file for more details
