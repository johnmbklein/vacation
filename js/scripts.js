var error = 0;
var name;
var age;
var gender;
var fishing;
var beaches;
var snow;
var hiking;
var museum;
var historical;
var showJackson;
var showBaja;
var showRome;
var showNYC;
var showAlaska;
var showOlympic;


var checkInput = function(input, target) {

  if (!input && input != "0") {   //check if field is blank
    $("#"+target).parent().addClass("has-error");
    $("#"+target+"-help-text").removeClass("hidden"); // show help text
    error ++;
  } else {    //remove error notification if field is filled in
    $("#"+target).parent().removeClass("has-error");
    $("#"+target+"-help-text").addClass("hidden")
  }
};

var computeResults = function(){ //this computes which results to display

  // check for snow vs beachs
  if (snow > beaches) {
    showJackson = true;
    showBaja = false;
  } else if (beaches > snow) {
    showBaja = true;
    showJackson = false;
  } else if (beaches === snow) {
    showJackson = true;
    showBaja = true;
  }

  //check for historical vs museum
  if (historical > museum) {
    showRome = true;
    showNYC = false;
  } else if (museum > historical) {
    showNYC = true;
    showRome = false;
  } else if (museum === historical) {
    showRome = true;
    showNYC = true;
  }

  //check for hiking vs fishing
  if (hiking > fishing) {
    showOlympic = true;
    showAlaska = false;
  } else if (fishing > hiking) {
    showAlaska = true;
    showOlympic = false;
  } else if (fishing === hiking) {
    showOlympic = true;
    showAlaska = true;
  }

  //check for hiking + fishing vs snow + beaches
  if (hiking + fishing > 2 * (snow + beaches)) {
    showOlympic = true;
    showAlaska = true;
    showJackson = false;
    showBaja = false;
  } else if (2 * (hiking + fishing) < snow + beaches) {
    showOlympic = false;
    showAlaska = false;
    showJackson = true;
    showBaja = true;
  }

  //check for snow + beaches vs historical + museum
  if (snow + beaches > 2 * (historical + museum)) {
    showJackson = true;
    showBaja = true;
    showRome = false;
    showNYC = false;
  } else if (2 * (snow + beaches) < historical + museum) {
    showJackson = false;
    showBaja = false;
    showRome = true;
    showNYC = true;
  }


  console.log("fishing: "+fishing);
  console.log("beaches: "+beaches);
  console.log("snow: "+snow);
  console.log("hiking: "+hiking);
  console.log("museum: "+museum);
  console.log("historical: "+historical);

};

var displayResults = function(){

  //add name to header
  $(".name").text(name);

  //show chosen elements
  if (showRome) {
    $("#rome").fadeIn();
  } else {
    $("#rome").addClass("nodisplay");
  }

  if (showNYC) {
    $("#nyc").fadeIn();
  } else {
    $("#nyc").addClass("nodisplay");
  }

  if (showOlympic) {
    $("#olympic").fadeIn();
  } else {
    $("#olympic").addClass("nodisplay");
  }

  if (showAlaska) {
    $("#alaska").fadeIn();
  } else {
    $("#alaska").addClass("nodisplay");
  }

  if (showJackson) {
    $("#jackson").fadeIn();
  } else {
    $("#jackson").addClass("nodisplay");
  }

  if (showBaja) {
    $("#baja").fadeIn();
  } else {
    $("#baja").addClass("nodisplay");
  }

}

$(document).ready(function(){

  // Process first question page
  $("#personal-info-form form").submit(function() {
    error = 0;

    //capture inputs
    name = $("#name").val();
    checkInput(name, "name"); //There must be a better way to do this, but how? (i.e., so I don't need to pass the parameter twice, as both a string and a value) Also, why does the value of this variable persist between page refreshes, while age and gender do not?
    age = parseInt($("#age").val());
    checkInput(age, "age");
    gender = $("#gender").val();
    checkInput(gender, "gender");

    //check for no errors => proceed to page 2
    if (error === 0) {
      $("#personal-info-form").addClass("hidden");
      $("#preference-info-form").removeClass("hidden");
    }
    event.preventDefault();
  });

  // Process second question page
  $("#preference-info-form form").submit(function() {
    error = 0;

    // catpure input
    fishing = parseInt($("input:radio[name=fishing]:checked").val());
    checkInput(fishing, "fishing");

    beaches = parseInt($("input:radio[name=beaches]:checked").val());
    checkInput(beaches, "beaches");

    snow = parseInt($("input:radio[name=snow]:checked").val());
    checkInput(snow, "snow");

    hiking = parseInt($("input:radio[name=hiking]:checked").val());
    checkInput(hiking, "hiking");

    museum = parseInt($("input:radio[name=museum]:checked").val());
    checkInput(museum, "museum");

    historical = parseInt($("input:radio[name=historical]:checked").val());
    checkInput(historical, "historical");

    //check for no errors ==> proceed to results
    if (error === 0) {
      $("#preference-info-form").addClass("hidden");
      $("#results-page").removeClass("hidden");
      console.log("page 2 no error");
      computeResults();
      displayResults();
    } else {
      console.log("page 2 error: "+error);
    }
    event.preventDefault();
  });

});
